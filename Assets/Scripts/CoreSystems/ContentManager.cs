﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Assertions;
using UnityEngine.ResourceManagement.AsyncOperations;
using Object = System.Object;

public class ContentManager
{
    private AsyncOperationHandle<IList<Object>> prewarmHandler;
        
    public async UniTask Init()
    {
        try
        {
            await Addressables.InitializeAsync();
            await UpdateCatalogs();
        }
        catch (Exception e)
        {
            Debug.LogError($"{e.Message}{Environment.NewLine}{e.StackTrace}");
        }
    }

    public void DeInit()
    {
        if (prewarmHandler.IsValid())
        {
            Addressables.Release(prewarmHandler);
            prewarmHandler = default;
        }
    }

    private async UniTask UpdateCatalogs()
    {
        var catalogsForUpdate = await Addressables.CheckForCatalogUpdates().Task;
        if (catalogsForUpdate.Count != 0)
        {
            await Addressables.UpdateCatalogs(catalogsForUpdate).Task;
        }
    }

    public async UniTask PreloadAssets(string label = "preload")
    {
        Assert.IsFalse(prewarmHandler.IsValid());
            
        prewarmHandler = Addressables.LoadAssetsAsync(label, (Action<Object>)null);
        await prewarmHandler.Task;
    }
        
    public async UniTask DownloadDependencies(string key)
    {
        await Addressables.DownloadDependenciesAsync(key);
    }

    public async UniTask<T> LoadAssetAsync<T>(string contentLabel, string assetPath)
    {
        Assert.IsFalse(string.IsNullOrEmpty(contentLabel));
            
        var targetType = typeof(T);
        string[] keys = {contentLabel, assetPath};
        var locations = await Addressables.LoadResourceLocationsAsync((IEnumerable)keys, Addressables.MergeMode.Intersection, targetType).Task;
        foreach (var location in locations)
        {
            if (location.PrimaryKey == assetPath)
            {
                var handle = Addressables.LoadAssetAsync<T>(location);
                await handle;
                return handle.Result;
            }
        }

        return default;
    }

    public AsyncOperationHandle<T> LoadAssetAsync<T>(string assetPath)
    {
        return Addressables.LoadAssetAsync<T>(assetPath);
    }

    public AsyncOperationHandle<T> LoadAssetAsync<T>(AssetReference reference)
    {
        return Addressables.LoadAssetAsync<T>(reference);
    }
        
    public T LoadAssetWithComponent<T>(string assetPath) where T: Component
    {
        var asset = Addressables.LoadAssetAsync<GameObject>(assetPath).WaitForCompletion();
        return asset.GetComponent<T>();
    }

    public async UniTask<IList<Object>> LoadAssetsAsync(string assetPath)
    {
        var handle = Addressables.LoadAssetsAsync(assetPath, (Action<Object>)null);
        await handle;
        return handle.Result;
    }
}