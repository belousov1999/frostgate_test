using System.Collections.Generic;
using Leopotam.EcsLite;
using OLS.Core;
using OLS.Features.Player.Data;
using OLS.Features.Unit.Game.Base;
using OLS.Features.Unit.Game.Middle;

namespace Features.Player.Game
{
    public class UnitSystemsBuilder : BaseFeatureBuilder
    {
        public override Dictionary<EcsWorld, string> GetFeatureWorlds()
        {
            return new Dictionary<EcsWorld, string>()
            {
                [new EcsWorld(UnitWorlds.UnitsConfig)] = UnitWorlds.Units,
            };
        }
        
        public override IEcsSystem[] GetDataProviders()
        {
            return new IEcsSystem[]
            {
            };
        }

        public override IEcsSystem[] GetBaseSystems()
        {
            return new IEcsSystem[]
            {
                new PlayerSpawnSystem(),
                new PlayerSaveSystem(),
            };
        }

        public override IEcsSystem[] GetMiddleSystems()
        {
            return new IEcsSystem[]
            {
                new PlayerClickToMoveSystem()
            };
        }

        public override IEcsSystem[] GetPostSystems()
        {
            return new IEcsSystem[]
            {
            };
        }
    }
}
