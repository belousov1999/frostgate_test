﻿using Features.BaseEcs.Game;
using Features.BaseEcs.Game.Middle;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Features.GameStates.Data.Events;
using OLS.Features.Player.Data;
using OLS.Features.Unit.Data.Components;
using OLS.Features.Unit.Data.Mono;
using UnityEngine;

namespace OLS.Features.Unit.Game.Base
{
    public class PlayerSpawnSystem : EventListenerSystem<LoadingStateEvent>
    {
        [EcsInject] private ContentManager contentManager;
        
        private EcsWorld unitsWorld;

        private const float MoveSpeed = 10;
        private const float RotationSpeed = 12;
        
        public override void Init(IEcsSystems systems)
        {
            base.Init(systems);
            unitsWorld = systems.GetWorld(UnitWorlds.Units);
        }

        protected override void OnEvent(int eventEntityId, LoadingStateEvent component)
        {
            var levelPrefab = contentManager.LoadAssetAsync<GameObject>("Location_1")
                .WaitForCompletion();
            Object.Instantiate(levelPrefab);
            
            var playerPrefab = contentManager.LoadAssetAsync<GameObject>("Player")
                .WaitForCompletion();

            var spawnPoint = PlayerSpawnPoint.Instance;
            
            CreatePlayer(unitsWorld, playerPrefab, spawnPoint.transform.position);
        }

        private void CreatePlayer(EcsWorld playerWorld, GameObject prefab, Vector3 position)
        {
            var playerGo = Object.Instantiate(prefab);

            var unit = playerWorld.NewComponentWithId<Player.Data.Components.Unit>();
            
            playerWorld.AddComponent<Unit.Data.Components.Player>(unit.EntityId);
            playerWorld.AddComponent<UnitMoveSpeed>(unit.EntityId).Value = MoveSpeed;
            playerWorld.AddComponent<UnitRotationSpeed>(unit.EntityId).Value = RotationSpeed;
            
            ref var unitTransform = ref playerWorld.AddComponent<UnitTransform>(unit.EntityId);
            unitTransform.Transform = playerGo.transform;

            unitTransform.Transform.position = position;
        }
    }
}