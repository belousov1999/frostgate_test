﻿using System;
using Leopotam.EcsLite;
using OLS.Features.BaseSave.Data;
using OLS.Features.Player.Data;
using OLS.Features.Unit.Data.Components;
using OLS.Features.Unit.Data.SaveData;

namespace OLS.Features.Unit.Game.Base
{
    public class PlayerSaveSystem : IEcsInitSystem, ISaveSystem
    {
        private EcsPool<UnitTransform> unitTransformPool;
        private EcsFilter playerTransformFilter;

        public void Init(IEcsSystems systems)
        {
            var unitsWorld = systems.GetWorld(UnitWorlds.Units);
            
            unitTransformPool = unitsWorld.GetPool<UnitTransform>();
            playerTransformFilter = unitsWorld.Filter<UnitTransform>().Inc<Unit.Data.Components.Player>().End(1);
        }

        public object GetSave()
        {
            var playerEntityId = playerTransformFilter.GetRawEntities()[0];
            var playerTransform = unitTransformPool.Get(playerEntityId).Transform;
            var playerPosition = playerTransform.position;
            var playerRotation = playerTransform.rotation;
            
            return new PlayerData
            {
                Position = new(playerPosition),
                Rotation = new(playerRotation)
            };
        }

        public Type GetSaveType()
        {
            return typeof(PlayerData);
        }

        public void Load(object data)
        {
            var saveData = (PlayerData)data;
            var playerEntityId = playerTransformFilter.GetRawEntities()[0];
            var playerTransform = unitTransformPool.Get(playerEntityId).Transform;
            playerTransform.SetPositionAndRotation(saveData.Position.GetUnityVector(), saveData.Rotation.GetUnityQuaternion());
        }
    }
}