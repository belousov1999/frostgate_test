﻿using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Features.GameStates.Data;
using OLS.Features.GameStates.Data.Components.States;
using OLS.Features.OrdersQueue.Game.Base;
using OLS.Features.Player.Data;
using OLS.Features.Unit.Data.Components;
using OLS.Features.Unit.Data.Mono;
using UnityEngine;

namespace OLS.Features.Unit.Game.Middle
{
    public class PlayerClickToMoveSystem : IEcsInitSystem, IEcsRunSystem
    {
        [EcsInject] private MoveOrderSystem moveOrderSystem;

        private Camera cachedCamera;
        private EcsFilter playerFilter;
        private EcsFilter gameplayStateFilter;

        public void Init(IEcsSystems systems)
        {
            cachedCamera = Camera.main;
            
            var unitsWorld = systems.GetWorld(UnitWorlds.Units);
            var gameStatesWorld = systems.GetWorld(GameStatesWorlds.GameStates);
            
            playerFilter = unitsWorld.Filter<Unit.Data.Components.Player>().End(1);
            gameplayStateFilter = gameStatesWorld.Filter<GameplayState>().End(1);
        }

        public void Run(IEcsSystems systems)
        {
            if (gameplayStateFilter.GetEntitiesCount() == 0)
            {
                return;
            }
            
            if (!Input.GetMouseButtonDown(0))
            {
                return;
            }

            Ray ray = cachedCamera.ScreenPointToRay(Input.mousePosition);
            if (!Physics.Raycast(ray, out var raycastHit, 1000f))
            {
                return;
            }
            
            if (raycastHit.transform == GroundView.Instance.transform)
            {
                var playerEntityId = playerFilter.GetRawEntities()[0];
                moveOrderSystem.AddOrder(playerEntityId, raycastHit.point);
            }
        }
    }
}