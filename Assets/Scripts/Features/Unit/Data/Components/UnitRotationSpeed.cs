﻿namespace OLS.Features.Unit.Data.Components
{
    public struct UnitRotationSpeed
    {
        public float Value;
    }
}