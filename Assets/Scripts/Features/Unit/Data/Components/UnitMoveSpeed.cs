﻿namespace OLS.Features.Unit.Data.Components
{
    public struct UnitMoveSpeed
    {
        public float Value;
    }
}