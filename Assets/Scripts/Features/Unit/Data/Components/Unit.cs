﻿using Features.BaseEcs.Data;

namespace OLS.Features.Player.Data.Components
{
    public struct Unit : IEcsComponentWithId
    {
        public int EntityId { get; set; }
    }
}