﻿using UnityEngine;

namespace OLS.Features.Unit.Data.Components
{
    public struct UnitTransform
    {
        public Transform Transform;
    }
}