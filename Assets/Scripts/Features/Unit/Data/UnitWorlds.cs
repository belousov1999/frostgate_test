using Leopotam.EcsLite;

namespace OLS.Features.Player.Data
{
    public static class UnitWorlds
    {
        public const string Units = nameof(Units);

        public static readonly EcsWorld.Config UnitsConfig = new(); 
    }
}