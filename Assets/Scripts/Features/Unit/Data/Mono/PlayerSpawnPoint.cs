﻿using UnityEngine;

namespace OLS.Features.Unit.Data.Mono
{
    //Not valid for big project, but for test example just OK
    public class PlayerSpawnPoint : MonoBehaviour
    {
        public static PlayerSpawnPoint Instance { get; private set; }

        public void Awake()
        {
            Instance = this;
        }

        private void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }
    }
}