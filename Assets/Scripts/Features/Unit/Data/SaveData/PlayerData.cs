﻿using OLS.Features.BaseSave.Data;

namespace OLS.Features.Unit.Data.SaveData
{
    public struct PlayerData
    {
        public Vec3 Position;
        public Vec4 Rotation;
    }
}