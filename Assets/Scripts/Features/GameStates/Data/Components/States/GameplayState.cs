﻿using Features.GameStates.UI.Views;

namespace OLS.Features.GameStates.Data.Components.States
{
    public struct GameplayState : IGameState
    {
        public int EntityId { get; set; }

        public GameplayScreenView GameplayScreen;
    }
}