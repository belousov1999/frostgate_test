﻿using UnityEngine;

namespace OLS.Features.GameStates.Data.Components.States
{
    public struct LoadingState : IGameState
    {
        public int EntityId { get; set; }
        
        public GameObject LoadingScreen;
        public float Timer;
    }
}