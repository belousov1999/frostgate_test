﻿namespace OLS.Features.GameStates.Data.Components.States
{
    public struct MenuState : IGameState
    {
        public int EntityId { get; set; }
        
        public MenuScreenView MenuScreen;
    }
}