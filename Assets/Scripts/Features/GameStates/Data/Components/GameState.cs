﻿using Features.BaseEcs.Data;

namespace OLS.Features.GameStates.Data.Components
{
    public struct GameState : IEcsComponentWithId
    {
        public int EntityId { get; set; }
        public int PrevStateEntityId;
        public int StateEntityId;
    }
}