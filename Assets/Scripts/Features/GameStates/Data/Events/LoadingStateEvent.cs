﻿using Features.BaseEcs.Data;

namespace OLS.Features.GameStates.Data.Events
{
    public struct LoadingStateEvent : IEventComponent
    {
        public int EntityId { get; set; }
        public string SenderSystem { get; set; }
        public int SenderEntityId { get; set; }
    }
}