using Leopotam.EcsLite;

namespace OLS.Features.GameStates.Data
{
    public static class GameStatesWorlds
    {
        public const string GameStates = nameof(GameStates);

        public static readonly EcsWorld.Config GameStatesConfig = new(); 
    }
}