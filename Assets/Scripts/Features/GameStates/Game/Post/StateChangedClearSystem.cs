﻿using Features.BaseEcs.Game;
using Leopotam.EcsLite;
using OLS.Features.GameStates.Data;
using OLS.Features.GameStates.Data.Components;

namespace OLS.Features.GameStates.Game.Post
{
    public class StateChangedClearSystem : IEcsInitSystem, IEcsRunSystem
    {
        private EcsWorld gameStatesWorld;
        private EcsPool<GameState> gameStatePool;
        private EcsPool<StateChangedFlag> stateChangedFlagPool;
        private EcsFilter gameStateChangedFilter;

        public void Init(IEcsSystems systems)
        {
            gameStatesWorld = systems.GetWorld(GameStatesWorlds.GameStates);
            
            gameStatePool = gameStatesWorld.GetPool<GameState>();
            stateChangedFlagPool = gameStatesWorld.GetPool<StateChangedFlag>();
            gameStateChangedFilter = gameStatesWorld.Filter<GameState>()
                .Inc<StateChangedFlag>().End(1);
        }

        public void Run(IEcsSystems systems)
        {
            gameStatePool.Iterate(gameStateChangedFilter, OnStateChangedClear);
        }

        private void OnStateChangedClear(int gameStateEntityId, ref GameState component)
        {
            if (component.PrevStateEntityId >= 0)
            {
                gameStatesWorld.DelEntity(component.PrevStateEntityId);
                component.PrevStateEntityId = -1;
            }
            
            stateChangedFlagPool.Del(gameStateEntityId);
        }
    }
}