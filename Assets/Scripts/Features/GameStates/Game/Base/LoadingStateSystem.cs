﻿using Features.BaseEcs.Data;
using Features.BaseEcs.Game.Base;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Features.GameStates.Data.Components.States;
using OLS.Features.GameStates.Data.Events;
using UnityEngine;

namespace OLS.Features.GameStates.Game.Base
{
    public class LoadingStateSystem : BaseStateSystem<LoadingState>
    {
        [EcsInject] private ContentManager contentManager;
        [EcsInject] private MenuStateSystem menuStateSystem;
        [EcsInject] private EventsManagerSystem eventsManagerSystem;
        
        private EcsPool<LoadingStateEvent> loadingStateEventPool;

        public override void Init(IEcsSystems systems)
        {
            base.Init(systems);

            var eventsWorld = systems.GetWorld(BaseEcsWorlds.Events);

            loadingStateEventPool = eventsWorld.GetPool<LoadingStateEvent>();
        }

        protected override void OnStateExit(int gameStateEntityId, ref LoadingState state)
        {
            Object.Destroy(state.LoadingScreen);
            state.LoadingScreen = null;
        }

        protected override void OnStateStart(int gameStateEntityId, ref LoadingState state)
        {
            if (state.LoadingScreen == null)
            {
                var loadingScreenPrefab = contentManager.LoadAssetAsync<GameObject>("CanvasLoadingScreen")
                    .WaitForCompletion();

                state.LoadingScreen = Object.Instantiate(loadingScreenPrefab);
            }
            
            eventsManagerSystem.SendEvent(loadingStateEventPool, state.EntityId, nameof(LoadingStateSystem));
        }

        protected override void OnStateUpdate(int gameStateEntityId, ref LoadingState state)
        {
            state.Timer += Time.deltaTime;
            if (state.Timer > 2)
            {
                menuStateSystem.SetGameState();
            }
        }
    }
}