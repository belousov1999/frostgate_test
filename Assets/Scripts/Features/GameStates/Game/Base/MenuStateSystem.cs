﻿using GoodCat.EcsLite.Shared;
using OLS.Features.GameStates.Data.Components.States;
using UnityEngine;

namespace OLS.Features.GameStates.Game.Base
{
    public class MenuStateSystem : BaseStateSystem<MenuState>
    {
        [EcsInject] private ContentManager contentManager;
        [EcsInject] private GameplayStateSystem gameplayStateSystem;
        
        protected override void OnStateExit(int gameStateEntityId, ref MenuState state)
        {
            Object.Destroy(state.MenuScreen.gameObject);
            state.MenuScreen = null;
        }

        protected override void OnStateStart(int gameStateEntityId, ref MenuState state)
        {
            if (state.MenuScreen == null)
            {
                var loadingScreenPrefab = contentManager.LoadAssetAsync<GameObject>("CanvasMenu")
                    .WaitForCompletion();

                state.MenuScreen = Object.Instantiate(loadingScreenPrefab)
                    .GetComponent<MenuScreenView>();
                
                state.MenuScreen.SubscribeClickToStart(OnSkipMenuScreen);
            }
        }

        private void OnSkipMenuScreen()
        {
            gameplayStateSystem.SetGameState();
        }

        protected override void OnStateUpdate(int gameStateEntityId, ref MenuState state)
        {
        }
    }
}