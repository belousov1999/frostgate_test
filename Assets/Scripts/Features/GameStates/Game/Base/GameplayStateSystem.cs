﻿using Features.GameStates.UI.Views;
using GoodCat.EcsLite.Shared;
using OLS.Features.GameStates.Data.Components.States;
using UnityEngine;

namespace OLS.Features.GameStates.Game.Base
{
    public class GameplayStateSystem : BaseStateSystem<GameplayState>
    {
        [EcsInject] private ContentManager contentManager;
        [EcsInject] private MenuStateSystem menuStateSystem;
        
        protected override void OnStateExit(int gameStateEntityId, ref GameplayState state)
        {
            Object.Destroy(state.GameplayScreen.gameObject);
            state.GameplayScreen = null;
        }

        protected override void OnStateStart(int gameStateEntityId, ref GameplayState state)
        {
            if (state.GameplayScreen == null)
            {
                var loadingScreenPrefab = contentManager.LoadAssetAsync<GameObject>("CanvasGameplay")
                    .WaitForCompletion();

                state.GameplayScreen = Object.Instantiate(loadingScreenPrefab)
                    .GetComponent<GameplayScreenView>();
                
                state.GameplayScreen.SubscribeExit(OnExitToMenu);
            }
        }

        private void OnExitToMenu()
        {
            menuStateSystem.SetGameState();
        }

        protected override void OnStateUpdate(int gameStateEntityId, ref GameplayState state)
        {
        }
    }
}