﻿using Features.BaseEcs.Game;
using Leopotam.EcsLite;
using OLS.Features.GameStates.Data;
using OLS.Features.GameStates.Data.Components;
using OLS.Features.GameStates.Data.Components.States;

namespace OLS.Features.GameStates.Game.Base
{
    public abstract class BaseStateSystem<T> : IEcsInitSystem, IEcsRunSystem, IEcsPostRunSystem
        where T : struct, IGameState
    {
        protected EcsWorld GameStatesWorld { get; private set; }
        protected EcsPool<GameState> GameStatePool { get; private set; }
        protected EcsPool<T> StatePool { get; private set; }
        protected EcsFilter GameStateFilter { get; private set; }
        
        private EcsPool<StateChangedFlag> stateChangedFlagPool;
        private EcsFilter gameStateChangedFilter;
        private EcsFilter stateFilter;

        public virtual void Init(IEcsSystems systems)
        {
            GameStatesWorld = systems.GetWorld(GameStatesWorlds.GameStates);

            GameStatePool = GameStatesWorld.GetPool<GameState>();
            stateChangedFlagPool = GameStatesWorld.GetPool<StateChangedFlag>();
            GameStateFilter = GameStatesWorld.Filter<GameState>().End(1);
            gameStateChangedFilter = GameStatesWorld.Filter<GameState>()
                .Inc<StateChangedFlag>().End(1);
            stateFilter = GameStatesWorld.FilterByType(typeof(T)).End(1);

            StatePool = (EcsPool<T>)GameStatesWorld.GetAndCreatePoolByType(typeof(T));
        }
        
        public void Run(IEcsSystems systems)
        {
            GameStatePool.Iterate(gameStateChangedFilter, OnGameStateChangedEnd);
        }

        public void PostRun(IEcsSystems systems)
        {
            if (stateFilter.GetEntitiesCount() > 0)
            {
                var gameStateEntityId = GameStateFilter.GetRawEntities()[0];
                var gameState = GameStatePool.Get(gameStateEntityId);

                var stateEntityId = stateFilter.GetRawEntities()[0];
                if (gameState.StateEntityId == stateEntityId)
                {
                    ref var state = ref StatePool.Get(gameState.StateEntityId);
                    OnStateUpdate(gameStateEntityId, ref state);
                }
            }
        }

        protected abstract void OnStateExit(int gameStateEntityId, ref T state);

        protected abstract void OnStateStart(int gameStateEntityId, ref T state);

        protected abstract void OnStateUpdate(int gameStateEntityId, ref T state);

        private void OnGameStateChangedStart(int gameStateEntityId, GameState gameState)
        {
            if (StatePool.Has(gameState.StateEntityId))
            {
                ref var state = ref StatePool.Get(gameState.StateEntityId);
                OnStateStart(gameState.StateEntityId, ref state);
            }
        }
        
        private void OnGameStateChangedEnd(int gameStateEntityId, GameState gameState)
        {
            if (gameState.PrevStateEntityId >= 0 && StatePool.Has(gameState.PrevStateEntityId))
            {
                ref var state = ref StatePool.Get(gameState.PrevStateEntityId);
                OnStateExit(gameState.PrevStateEntityId, ref state);
            }
        }

        public void SetGameState()
        {
            var gameStateEntityId = GameStateFilter.GetRawEntities()[0];
            if (stateChangedFlagPool.Has(gameStateEntityId))
            {
                return;
            }

            ref var gameState = ref GameStatePool.Get(gameStateEntityId);

            if (gameState.StateEntityId >= 0)
            {
                gameState.PrevStateEntityId = gameState.StateEntityId;
            }

            gameState.StateEntityId = StatePool.NewWithId().EntityId;

            stateChangedFlagPool.Add(gameStateEntityId);

            OnGameStateChangedStart(gameStateEntityId, gameState);
        }
    }
}