﻿using Features.BaseEcs.Game;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Features.GameStates.Data;
using OLS.Features.GameStates.Data.Components;
using OLS.Features.GameStates.Data.Components.States;

namespace OLS.Features.GameStates.Game.Base
{
    public class StatesManagerSystem : IEcsInitSystem
    {
        [EcsInject] private LoadingStateSystem loadingStateSystem;
        
        private EcsWorld gameStatesWorld;
        private EcsPool<GameState> gameStatePool;

        public void Init(IEcsSystems systems)
        {
            gameStatesWorld = systems.GetWorld(GameStatesWorlds.GameStates);

            gameStatePool = gameStatesWorld.GetPool<GameState>();

            CreateGameState();
        }

        private void CreateGameState()
        {
            ref var gameState = ref gameStatePool.NewWithId();
            gameState.PrevStateEntityId = -1;
            gameState.StateEntityId = -1;
            
            loadingStateSystem.SetGameState();
        }
    }
}