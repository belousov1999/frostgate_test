using System.Collections.Generic;
using Leopotam.EcsLite;
using OLS.Core;
using OLS.Features.GameStates.Data;
using OLS.Features.GameStates.Game.Base;
using OLS.Features.GameStates.Game.Post;

namespace OLS.Features.GameStates.Game
{
    public class GameStatesSystemsBuilder : BaseFeatureBuilder
    {
        public override Dictionary<EcsWorld, string> GetFeatureWorlds()
        {
            return new Dictionary<EcsWorld, string>()
            {
                [new EcsWorld(GameStatesWorlds.GameStatesConfig)] = GameStatesWorlds.GameStates,
            };
        }
        
        public override IEcsSystem[] GetDataProviders()
        {
            return new IEcsSystem[]
            {
            };
        }

        public override IEcsSystem[] GetBaseSystems()
        {
            return new IEcsSystem[]
            {
                new LoadingStateSystem(),
                new MenuStateSystem(),
                new GameplayStateSystem(),
                new StatesManagerSystem(),
            };
        }

        public override IEcsSystem[] GetMiddleSystems()
        {
            return new IEcsSystem[]
            {
            };
        }

        public override IEcsSystem[] GetPostSystems()
        {
            return new IEcsSystem[]
            {
                new StateChangedClearSystem(),
            };
        }
    }
}
