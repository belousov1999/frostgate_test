using System;
using UnityEngine;
using UnityEngine.UI;

public class MenuScreenView : MonoBehaviour
{
    [SerializeField] private Button clickToStartButton;

    public void SubscribeClickToStart(Action callback)
    {
        clickToStartButton.onClick.AddListener(()=>callback?.Invoke());
    }
}
