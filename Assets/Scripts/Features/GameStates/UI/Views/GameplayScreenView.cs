﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Features.GameStates.UI.Views
{
    public class GameplayScreenView : MonoBehaviour
    {
        [SerializeField] private Button exitButton;

        public void SubscribeExit(Action callback)
        {
            exitButton.onClick.AddListener(() => callback?.Invoke());
        }
    }
}