using Leopotam.EcsLite;

namespace OLS.Features.OrdersQueue.Data
{
    public static class OrdersQueueWorlds
    {
        public const string OrdersQueue = nameof(OrdersQueue);

        public static readonly EcsWorld.Config OrdersQueueConfig = new(); 
    }
}