﻿using Newtonsoft.Json;
using OLS.Features.BaseSave.Data;
using UnityEngine;

namespace OLS.Features.OrdersQueue.Data.Components
{
    public struct MoveOrder : IOrderComponent
    {
        [JsonIgnore]
        public int EntityId { get; set; }
        
        public Vec3 Position;

        public MoveOrder(MoveOrder order)
        {
            EntityId = 0;
            Position = order.Position;
        }
    }
}