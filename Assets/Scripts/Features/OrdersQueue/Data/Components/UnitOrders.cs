﻿using System.Collections.Generic;
using Features.BaseEcs.Data;

namespace OLS.Features.OrdersQueue.Data.Components
{
    public struct UnitOrders : IEcsComponentWithId
    {
        public int EntityId { get; set; }
        public Queue<int> Orders;
    }
}