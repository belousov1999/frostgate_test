﻿using System;
using OLS.Features.OrdersQueue.Data.Components;

namespace OLS.Features.OrdersQueue.Data.SaveData
{
    public struct OrderSave
    {
        public Type OrderType;
        public IOrderComponent Order;
    }
}