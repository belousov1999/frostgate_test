﻿using OLS.Features.OrdersQueue.Data.Components;

namespace OLS.Features.OrdersQueue.Data.SaveData
{
    public struct UnitOrdersSave
    {
        public OrderSave[] Orders;
    }
}