﻿using System.Collections.Generic;
using Features.BaseEcs.Game;
using Leopotam.EcsLite;
using OLS.Features.GameStates.Data;
using OLS.Features.GameStates.Data.Components.States;
using OLS.Features.OrdersQueue.Data;
using OLS.Features.OrdersQueue.Data.Components;
using OLS.Features.OrdersQueue.Game.Base;
using OLS.Features.Player.Data;

namespace OLS.Features.OrdersQueue.Game.Middle
{
    public class OrdersUpdateSystem : IEcsInitSystem, IEcsRunSystem
    {
        private IEnumerable<IOrderSystem> orderSystems;
        private EcsWorld ordersQueueWorld;
        private EcsPool<UnitOrders> unitOrdersPool;
        private EcsFilter unitOrdersFilter;
        private EcsFilter gameplayStateFilter;

        public void Init(IEcsSystems systems)
        {
            orderSystems = systems.GetSystems<IOrderSystem>();

            ordersQueueWorld = systems.GetWorld(OrdersQueueWorlds.OrdersQueue);
            var unitsWorld = systems.GetWorld(UnitWorlds.Units);
            var gameStatesWorld = systems.GetWorld(GameStatesWorlds.GameStates);

            unitOrdersPool = unitsWorld.GetPool<UnitOrders>();
            
            unitOrdersFilter = unitsWorld.Filter<UnitOrders>().End();
            gameplayStateFilter = gameStatesWorld.Filter<GameplayState>().End(1);
        }

        public void Run(IEcsSystems systems)
        {
            if (gameplayStateFilter.GetEntitiesCount() == 0)
            {
                return;
            }
            
            unitOrdersPool.Iterate(unitOrdersFilter, OnOrdersUpdate);
        }

        private void OnOrdersUpdate(int unitEntityId, UnitOrders unitOrders)
        {
            //Can be optimized with extra flag for empty queue check
            if (unitOrders.Orders.Count == 0)
            {
                return;
            }

            var orderEntityId = unitOrders.Orders.Peek();
            
            foreach (var orderSystem in orderSystems)
            {
                if (orderSystem.IsNeedUpdate(unitEntityId, orderEntityId))
                {
                    var successful = orderSystem.UpdateOrder(unitEntityId, orderEntityId);
                    if (successful)
                    {
                        unitOrders.Orders.Dequeue();
                        ordersQueueWorld.DelEntity(orderEntityId);
                    }

                    break;
                }
            }
        }
    }
}