﻿using System;
using Leopotam.EcsLite;
using OLS.Features.OrdersQueue.Data.Components;

namespace OLS.Features.OrdersQueue.Game.Base
{
    public interface IOrderSystem : IEcsSystem
    {
        public bool IsValidOrderType(Type orderType);

        public int CopyOrder(int unitEntityId, IOrderComponent orderComponent);
            
        public bool TryGetOrder(int orderEntityId, out IOrderComponent orderComponent);
        
        public bool IsNeedUpdate(int unitEntityId, int orderEntityId);
        
        public bool UpdateOrder(int unitEntityId, int orderEntityId);
    }
}