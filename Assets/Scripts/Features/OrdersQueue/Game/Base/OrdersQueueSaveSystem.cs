﻿using System;
using System.Collections.Generic;
using System.Linq;
using Features.BaseEcs.Game;
using Leopotam.EcsLite;
using Newtonsoft.Json;
using OLS.Features.BaseSave.Data;
using OLS.Features.OrdersQueue.Data.Components;
using OLS.Features.OrdersQueue.Data.SaveData;
using OLS.Features.Player.Data;
using OLS.Features.Unit.Data.Components;
using UnityEngine;

namespace OLS.Features.OrdersQueue.Game.Base
{
    public class OrdersQueueSaveSystem : IEcsInitSystem, ISaveSystem
    {
        private List<IOrderSystem> orderSystems;
        private EcsPool<UnitOrders> unitOrdersPool;
        private EcsFilter unitOrdersFilter;

        public void Init(IEcsSystems systems)
        {
            orderSystems = systems.GetSystems<IOrderSystem>().ToList();

            var unitsWorld = systems.GetWorld(UnitWorlds.Units);

            unitOrdersPool = unitsWorld.GetPool<UnitOrders>();
            unitOrdersFilter = unitsWorld.Filter<UnitOrders>().Inc<Unit.Data.Components.Player>().End(1);

            //Test();
        }

        private void Test()
        {
            var moveOrder = new MoveOrder()
            {
                Position = new Vec3()
                {
                    X = 1,
                    Y = 2,
                    Z = 3
                }
            };

            var save = new OrderSave
            {
                OrderType = moveOrder.GetType(),
                Order = moveOrder
            };
            
            var jsonSave = JsonConvert.SerializeObject(save, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            });

            save = JsonConvert.DeserializeObject<OrderSave>(jsonSave, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            });
            
            moveOrder = (MoveOrder)save.Order;
            Debug.Log("Test Position: "+moveOrder.Position);
        }

        //Not valid for multiple units save queue but for example just OK
        public object GetSave()
        {
            var unitEntityId = unitOrdersFilter.GetRawEntities()[0];
            if (unitOrdersPool.Has(unitEntityId) == false)
            {
                return null;
            }

            var unitOrders = unitOrdersPool.Get(unitEntityId);
            if (unitOrders.Orders.Count == 0)
            {
                return null;
            }

            var ordersCount = unitOrders.Orders.Count;
            var orders = new OrderSave[ordersCount];
            for (int i = 0; i < ordersCount; i++)
            {
                var orderEntityId = unitOrders.Orders.Dequeue();
                foreach (var orderSystem in orderSystems)
                {
                    if (orderSystem.TryGetOrder(orderEntityId, out var orderComponent))
                    {
                        orders[i] = new OrderSave
                        {
                            OrderType = orderComponent.GetType(),
                            Order = orderComponent
                        };
                        break;
                    }
                }
            }

            return new UnitOrdersSave
            {
                Orders = orders
            };
        }

        public Type GetSaveType()
        {
            return typeof(UnitOrdersSave);
        }

        public void Load(object data)
        {
            var saveData = (UnitOrdersSave)data;
            if (saveData.Orders == null || saveData.Orders.Length == 0)
            {
                return;
            }

            var unitEntityId = unitOrdersFilter.GetRawEntities()[0];
            
            foreach (var orderSave in saveData.Orders)
            {
                foreach (var orderSystem in orderSystems)
                {
                    if (orderSystem.IsValidOrderType(orderSave.OrderType))
                    {
                        orderSystem.CopyOrder(unitEntityId, orderSave.Order);
                        break;
                    }
                }
            }
        }
    }
}