﻿using System;
using System.Collections.Generic;
using Features.BaseEcs.Game;
using Leopotam.EcsLite;
using OLS.Features.OrdersQueue.Data;
using OLS.Features.OrdersQueue.Data.Components;
using OLS.Features.Player.Data;

namespace OLS.Features.OrdersQueue.Game.Base
{
    public abstract class BaseOrderSystem<T> : IEcsInitSystem, IOrderSystem
        where T : struct, IOrderComponent
    {
        protected EcsWorld UnitsWorld { get; private set; }
        protected EcsWorld OrdersQueueWorld { get; private set; }
        protected EcsPool<T> OrdersPool { get; private set; }
        private EcsPool<UnitOrders> unitOrdersPool;

        public virtual void Init(IEcsSystems systems)
        {
            UnitsWorld = systems.GetWorld(UnitWorlds.Units);
            OrdersQueueWorld = systems.GetWorld(OrdersQueueWorlds.OrdersQueue);

            unitOrdersPool = UnitsWorld.GetPool<UnitOrders>();
            OrdersPool = (EcsPool<T>)OrdersQueueWorld.GetAndCreatePoolByType(typeof(T));
        }

        protected ref T AddOrder(int unitEntityId)
        {
            var orderEntityId = OrdersQueueWorld.NewEntity();
            ref var order = ref OrdersPool.Add(orderEntityId);
            order.EntityId = orderEntityId;

            if (unitOrdersPool.Has(unitEntityId))
            {
                unitOrdersPool.Get(unitEntityId).Orders.Enqueue(orderEntityId);
                return ref order;
            }

            ref var unitOrders = ref unitOrdersPool.Add(unitEntityId);
            unitOrders.Orders = new Queue<int>();
            unitOrders.Orders.Enqueue(orderEntityId);
            return ref order;
        }

        public bool IsValidOrderType(Type orderType)
        {
            return orderType == typeof(T);
        }

        public virtual int CopyOrder(int unitEntityId, IOrderComponent orderComponent)
        {
            return AddOrder(unitEntityId).EntityId;
        }

        public bool TryGetOrder(int orderEntityId, out IOrderComponent orderComponent)
        {
            if (OrdersPool.Has(orderEntityId) == false)
            {
                orderComponent = default;
                return false;
            }

            orderComponent = OrdersPool.Get(orderEntityId);
            return true;
        }

        public bool IsNeedUpdate(int unitEntityId, int orderEntityId)
        {
            return OrdersPool.Has(orderEntityId);
        }

        public abstract bool UpdateOrder(int unitEntityId, int orderEntityId);
    }
}