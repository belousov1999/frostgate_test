﻿using Leopotam.EcsLite;
using OLS.Features.BaseSave.Data;
using OLS.Features.OrdersQueue.Data.Components;
using OLS.Features.Unit.Data.Components;
using UnityEngine;

namespace OLS.Features.OrdersQueue.Game.Base
{
    public class MoveOrderSystem : BaseOrderSystem<MoveOrder>
    {
        private EcsPool<UnitTransform> unitTransformPool;
        private EcsPool<UnitMoveSpeed> unitMoveSpeedPool;
        private EcsPool<UnitRotationSpeed> unitRotationSpeedPool;

        private const float MinDistance = 1;
        
        public override void Init(IEcsSystems systems)
        {
            base.Init(systems);

            unitTransformPool = UnitsWorld.GetPool<UnitTransform>();
            unitMoveSpeedPool = UnitsWorld.GetPool<UnitMoveSpeed>();
            unitRotationSpeedPool = UnitsWorld.GetPool<UnitRotationSpeed>();
        }

        public void AddOrder(int unitEntityId, Vector3 position)
        {
            ref var order = ref AddOrder(unitEntityId);
            order.Position = new Vec3(position);
        }

        public override int CopyOrder(int unitEntityId, IOrderComponent orderComponent)
        {
            var orderEntityId = base.CopyOrder(unitEntityId, orderComponent);
            var moveOrder = (MoveOrder)orderComponent;
            ref var order = ref OrdersPool.Get(orderEntityId);
            order.Position = moveOrder.Position;

            return orderEntityId;
        }

        public override bool UpdateOrder(int unitEntityId, int orderEntityId)
        {
            var unitTransform = unitTransformPool.Get(unitEntityId).Transform;
            var moveOrder = OrdersPool.Get(orderEntityId);
            var moveSpeed = unitMoveSpeedPool.Get(unitEntityId).Value;
            var rotationSpeed = unitRotationSpeedPool.Get(unitEntityId).Value;

            bool success = UpdatePosition(unitTransform, moveSpeed, moveOrder.Position.GetUnityVector());
            success &= UpdateRotation(unitTransform, rotationSpeed, moveOrder.Position.GetUnityVector());

            return success;
        }

        private bool UpdatePosition(Transform unitTransform, float moveSpeed, Vector3 targetPosition)
        {
            var updatedPosition = unitTransform.position;
            updatedPosition = Vector3.MoveTowards(updatedPosition, targetPosition,
                moveSpeed * Time.deltaTime);
            
            //We can use any A* framework. For example just moving towards
            unitTransform.position = updatedPosition;
            
            //Can be optimized, but for 1 unit its OK
            return Vector3.Distance(updatedPosition, targetPosition) <= MinDistance;
        }

        private bool UpdateRotation(Transform unitTransform, float rotationSpeed, Vector3 targetPosition)
        {
            Vector3 targetDirection = (targetPosition - unitTransform.position).normalized;
            var unitRotation = unitTransform.rotation;

            var targetRotation = targetDirection == Vector3.zero ? 
                unitRotation : 
                Quaternion.LookRotation(targetDirection);
            
            unitRotation = Quaternion.Slerp(unitRotation, targetRotation, rotationSpeed * Time.deltaTime);
            unitTransform.rotation = unitRotation;

            return Quaternion.Angle(unitRotation, targetRotation) < 0.001f;
        }
    }
}