using System.Collections.Generic;
using Leopotam.EcsLite;
using OLS.Core;
using OLS.Features.OrdersQueue.Data;
using OLS.Features.OrdersQueue.Game.Base;
using OLS.Features.OrdersQueue.Game.Middle;

namespace OLS.Features.OrdersQueue.Game
{
    public class OrdersQueueSystemsBuilder : BaseFeatureBuilder
    {
        public override Dictionary<EcsWorld, string> GetFeatureWorlds()
        {
            return new Dictionary<EcsWorld, string>()
            {
                [new EcsWorld(OrdersQueueWorlds.OrdersQueueConfig)] = OrdersQueueWorlds.OrdersQueue,
            };
        }
        
        public override IEcsSystem[] GetDataProviders()
        {
            return new IEcsSystem[]
            {
            };
        }

        public override IEcsSystem[] GetBaseSystems()
        {
            return new IEcsSystem[]
            {
                new MoveOrderSystem(),
                new OrdersQueueSaveSystem(),
            };
        }

        public override IEcsSystem[] GetMiddleSystems()
        {
            return new IEcsSystem[]
            {
                new OrdersUpdateSystem()
            };
        }

        public override IEcsSystem[] GetPostSystems()
        {
            return new IEcsSystem[]
            {
            };
        }
    }
}
