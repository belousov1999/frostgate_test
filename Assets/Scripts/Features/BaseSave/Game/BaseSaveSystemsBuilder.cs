using System.Collections.Generic;
using Leopotam.EcsLite;
using OLS.Core;
using OLS.Features.BaseSave.Data;
using OLS.Features.BaseSave.Game.Base;
using OLS.Features.BaseSave.Game.Middle;
using OLS.Features.BaseSave.Game.Post;

namespace Features.BaseSave.Game
{
    public class BaseSaveSystemsBuilder : BaseFeatureBuilder
    {
        public override Dictionary<EcsWorld, string> GetFeatureWorlds()
        {
            return new Dictionary<EcsWorld, string>()
            {
            };
        }
        
        public override IEcsSystem[] GetDataProviders()
        {
            return new IEcsSystem[]
            {
            };
        }

        public override IEcsSystem[] GetBaseSystems()
        {
            return new IEcsSystem[]
            {
                new PlayerPrefsDataProvider(),
            };
        }

        public override IEcsSystem[] GetMiddleSystems()
        {
            return new IEcsSystem[]
            {
                new LoadGameStatesSystem()
            };
        }

        public override IEcsSystem[] GetPostSystems()
        {
            return new IEcsSystem[]
            {
                new SaveGameStatesSystem()
            };
        }
    }
}
