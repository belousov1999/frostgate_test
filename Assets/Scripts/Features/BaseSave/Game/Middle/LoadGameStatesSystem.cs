﻿using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Features.BaseEcs.Game;
using Features.BaseEcs.Game.Middle;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using Newtonsoft.Json;
using OLS.Features.BaseSave.Data;
using OLS.Features.BaseSave.Game.Base;
using OLS.Features.BaseSave.Game.Post;
using OLS.Features.GameStates.Data.Events;
using UnityEngine;

namespace OLS.Features.BaseSave.Game.Middle
{
    public class LoadGameStatesSystem : EventListenerSystem<LoadingStateEvent>
    {
        [EcsInject] private PlayerPrefsDataProvider playerPrefsDataProvider;
        
        private List<ISaveSystem> saveSystems;

        public override void Init(IEcsSystems systems)
        {
            base.Init(systems);
            saveSystems = systems.GetSystems<ISaveSystem>().ToList();
        }

        protected override void OnEvent(int eventEntityId, LoadingStateEvent component)
        {
            //Here we can load from web, or any source this json file
            playerPrefsDataProvider.GetSave()
                .ContinueWith(OnLoadSave).Forget();
        }

        private void OnLoadSave(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                return;
            }
            
            var allStatesSave = JsonConvert.DeserializeObject<AllStatesSave>(json, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            });

            foreach (var stateSave in allStatesSave.States)
            {
                if (string.IsNullOrEmpty(stateSave.Json) || stateSave.Json == "null")
                {
                    continue;
                }
                
                foreach (var saveSystem in saveSystems)
                {
                    if (saveSystem.GetType().FullName == stateSave.System)
                    {
                        var saveType = saveSystem.GetSaveType();
                        
                        var save = JsonConvert.DeserializeObject(stateSave.Json, saveType, new JsonSerializerSettings
                        {
                            TypeNameHandling = TypeNameHandling.Auto
                        });
                        saveSystem.Load(save);
                    }
                }
            }
        }
    }
}