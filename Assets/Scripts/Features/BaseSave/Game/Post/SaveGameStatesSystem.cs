﻿using System.Collections.Generic;
using System.Linq;
using Features.BaseEcs.Game;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using Newtonsoft.Json;
using OLS.Features.BaseSave.Data;
using OLS.Features.BaseSave.Game.Base;
using UnityEngine;

namespace OLS.Features.BaseSave.Game.Post
{
    public class SaveGameStatesSystem : IEcsInitSystem
    {
        [EcsInject] private PlayerPrefsDataProvider playerPrefsDataProvider;
        
        private List<ISaveSystem> saveSystems;
        
        public void Init(IEcsSystems systems)
        {
            Application.quitting += OnApplicationQuit;

            saveSystems = systems.GetSystems<ISaveSystem>().ToList();
        }

        private void OnApplicationQuit()
        {
            List<StateSave> stateSaves = new(saveSystems.Count);

            foreach (var saveSystem in saveSystems)
            {
                var save = saveSystem.GetSave();
                var jsonSave = JsonConvert.SerializeObject(save, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto
                });
                stateSaves.Add(new StateSave
                {
                    System = saveSystem.GetType().FullName,
                    Json = jsonSave
                });
            }

            var allStatesSaveJson = JsonConvert.SerializeObject(new AllStatesSave
            {
                States = stateSaves.ToArray()
            }, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            });
            
            playerPrefsDataProvider.Save(allStatesSaveJson);
        }
    }
}