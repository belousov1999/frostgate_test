﻿using Cysharp.Threading.Tasks;

namespace OLS.Features.BaseSave.Game.Base
{
    public interface ISaveDataProvider
    {
        public UniTask<string> GetSave();

        public void Save(string json);
    }
}