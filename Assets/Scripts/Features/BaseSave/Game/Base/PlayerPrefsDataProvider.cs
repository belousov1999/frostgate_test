﻿using Cysharp.Threading.Tasks;
using Leopotam.EcsLite;
using UnityEngine;

namespace OLS.Features.BaseSave.Game.Base
{
    public class PlayerPrefsDataProvider : IEcsSystem, ISaveDataProvider
    {
        public const string SaveKey = "Save";
        
        public async UniTask<string> GetSave()
        {
            if (PlayerPrefs.HasKey(SaveKey) == false)
            {
                return null;
            }
            
            var saveJson = PlayerPrefs.GetString(SaveKey);
            return saveJson;
        }

        public void Save(string json)
        {
            PlayerPrefs.SetString(SaveKey, json);
            PlayerPrefs.Save();
        }
    }
}