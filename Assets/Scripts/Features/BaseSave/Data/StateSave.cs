﻿using System;

namespace OLS.Features.BaseSave.Data
{
    [Serializable]
    public struct StateSave
    {
        public string System;
        public string Json;
    }
}