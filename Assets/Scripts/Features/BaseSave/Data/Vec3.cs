﻿using UnityEngine;

namespace OLS.Features.BaseSave.Data
{
    public struct Vec3
    {
        public float X;
        public float Y;
        public float Z;

        public Vec3(Vector3 unityVector)
        {
            X = unityVector.x;
            Y = unityVector.y;
            Z = unityVector.z;
        }
        
        public Vector3 GetUnityVector()
        {
            return new Vector3(X, Y, Z);
        }
    }
}