﻿using System;
using Leopotam.EcsLite;

namespace OLS.Features.BaseSave.Data
{
    public interface ISaveSystem : IEcsSystem
    {
        public object GetSave();

        public Type GetSaveType();
        
        public void Load(object data);
    }
}