﻿using UnityEngine;

namespace OLS.Features.BaseSave.Data
{
    public struct Vec4
    {
        public float X;
        public float Y;
        public float Z;
        public float W;

        public Vec4(Quaternion unityRotation)
        {
            X = unityRotation.x;
            Y = unityRotation.y;
            Z = unityRotation.z;
            W = unityRotation.w;
        }
        
        public Quaternion GetUnityQuaternion()
        {
            return new Quaternion(X, Y, Z, W);
        }
    }
}