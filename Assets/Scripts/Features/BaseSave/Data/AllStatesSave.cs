﻿using System;

namespace OLS.Features.BaseSave.Data
{
    [Serializable]
    public struct AllStatesSave
    {
        public StateSave[] States;
    }
}