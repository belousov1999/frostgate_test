using System.Collections.Generic;
using Leopotam.EcsLite;
using OLS.Core;
using OLS.Features.Camera.Data;
using OLS.Features.Camera.Game.Base;
using OLS.Features.Camera.Game.Middle;
using OLS.Features.Camera.Game.Post;

namespace OLS.Features.Camera.Game
{
    public class CameraSystemsBuilder : BaseFeatureBuilder
    {
        public override Dictionary<EcsWorld, string> GetFeatureWorlds()
        {
            return new Dictionary<EcsWorld, string>()
            {
                [new EcsWorld(CameraWorlds.CameraConfig)] = CameraWorlds.Camera,
            };
        }
        
        public override IEcsSystem[] GetDataProviders()
        {
            return new IEcsSystem[]
            {
            };
        }

        public override IEcsSystem[] GetBaseSystems()
        {
            return new IEcsSystem[]
            {
                new CameraInitSystem(),
                new CameraSaveSystem(),
            };
        }

        public override IEcsSystem[] GetMiddleSystems()
        {
            return new IEcsSystem[]
            {
            };
        }

        public override IEcsSystem[] GetPostSystems()
        {
            return new IEcsSystem[]
            {
                new CameraMoveSystem()
            };
        }
    }
}
