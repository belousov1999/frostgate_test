﻿using Features.BaseEcs.Game;
using Leopotam.EcsLite;
using OLS.Features.Camera.Data;
using OLS.Features.Camera.Data.Components;
using OLS.Features.Player.Data;
using OLS.Features.Player.Data.Components;
using OLS.Features.Unit.Data.Components;
using UnityEngine;

namespace OLS.Features.Camera.Game.Post
{
    public class CameraMoveSystem : IEcsInitSystem, IEcsPostRunSystem
    {
        private EcsPool<CameraTransform> cameraTransformPool;
        private EcsPool<UnitTransform> unitTransformPool;
        private EcsFilter cameraTransformFilter;
        private EcsFilter playerTransformFilter;

        private static readonly Vector3 CameraOffset = new Vector3(0, 18, -10);
        private const float CameraSpeed = 7;
        
        public void Init(IEcsSystems systems)
        {
            var cameraWorld = systems.GetWorld(CameraWorlds.Camera);
            var unitsWorld = systems.GetWorld(UnitWorlds.Units);

            cameraTransformPool = cameraWorld.GetPool<CameraTransform>();
            cameraTransformFilter = cameraWorld.Filter<CameraTransform>().End(1);
            
            unitTransformPool = unitsWorld.GetPool<UnitTransform>();
            playerTransformFilter = unitsWorld.Filter<UnitTransform>().Inc<Unit.Data.Components.Player>().End(1);
        }

        public void PostRun(IEcsSystems systems)
        {
            unitTransformPool.Iterate(playerTransformFilter, OnCameraPositionUpdate);
        }

        private void OnCameraPositionUpdate(int playerEntityId, UnitTransform playerTransform)
        {
            var cameraEntities = cameraTransformFilter.GetRawEntities();
            var cameraTransform = cameraTransformPool.Get(cameraEntities[0]);
            
            var targetPosition = playerTransform.Transform.position;
            targetPosition += CameraOffset;

            cameraTransform.Transform.position = Vector3.MoveTowards(cameraTransform.Transform.position, targetPosition,
                CameraSpeed * Time.deltaTime);;
        }
    }
}