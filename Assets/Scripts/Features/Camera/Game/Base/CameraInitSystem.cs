﻿using Features.BaseEcs.Game;
using Leopotam.EcsLite;
using OLS.Features.Camera.Data;
using OLS.Features.Camera.Data.Components;

namespace OLS.Features.Camera.Game.Base
{
    public class CameraInitSystem : IEcsInitSystem
    {
        public void Init(IEcsSystems systems)
        {
            var cameraWorld = systems.GetWorld(CameraWorlds.Camera);

            var camera = cameraWorld.NewComponentWithId<Data.Components.Camera>();
            ref var cameraTransform = ref cameraWorld.AddComponent<CameraTransform>(camera.EntityId);
            cameraTransform.Transform = UnityEngine.Camera.main.transform;
        }
    }
}