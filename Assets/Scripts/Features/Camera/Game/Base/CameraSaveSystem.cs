﻿using System;
using Leopotam.EcsLite;
using OLS.Features.BaseSave.Data;
using OLS.Features.Camera.Data;
using OLS.Features.Camera.Data.Components;
using OLS.Features.Camera.Data.SaveData;
using UnityEngine;

namespace OLS.Features.Camera.Game.Middle
{
    public class CameraSaveSystem : IEcsInitSystem, ISaveSystem
    {
        private EcsPool<CameraTransform> cameraTransformPool;
        private EcsFilter cameraTransformFilter;

        public void Init(IEcsSystems systems)
        {
            var cameraWorld = systems.GetWorld(CameraWorlds.Camera);
            
            cameraTransformPool = cameraWorld.GetPool<CameraTransform>();
            cameraTransformFilter = cameraWorld.Filter<CameraTransform>().End(1);
        }

        public object GetSave()
        {
            var cameraEntityId = cameraTransformFilter.GetRawEntities()[0];
            var cameraTransform = cameraTransformPool.Get(cameraEntityId).Transform;
            var position = cameraTransform.position;
            
            return new CameraSaveData
            {
                Position = new Vec3(position)
            };
        }

        public Type GetSaveType()
        {
            return typeof(CameraSaveData);
        }

        public void Load(object data)
        {
            var save = (CameraSaveData)data;
            var cameraEntityId = cameraTransformFilter.GetRawEntities()[0];
            var cameraTransform = cameraTransformPool.Get(cameraEntityId).Transform;

            cameraTransform.position = save.Position.GetUnityVector();
        }
    }
}