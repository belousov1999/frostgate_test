﻿using Features.BaseEcs.Data;

namespace OLS.Features.Camera.Data.Components
{
    public struct Camera : IEcsComponentWithId
    {
        public int EntityId { get; set; }
    }
}