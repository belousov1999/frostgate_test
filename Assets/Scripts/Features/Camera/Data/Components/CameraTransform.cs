﻿using UnityEngine;

namespace OLS.Features.Camera.Data.Components
{
    public struct CameraTransform
    {
        public Transform Transform;
    }
}