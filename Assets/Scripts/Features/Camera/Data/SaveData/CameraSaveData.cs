﻿using OLS.Features.BaseSave.Data;
using UnityEngine;

namespace OLS.Features.Camera.Data.SaveData
{
    public struct CameraSaveData
    {
        public Vec3 Position;
    }
}