using Leopotam.EcsLite;

namespace OLS.Features.Camera.Data
{
    public static class CameraWorlds
    {
        public const string Camera = nameof(Camera);

        public static readonly EcsWorld.Config CameraConfig = new(); 
    }
}