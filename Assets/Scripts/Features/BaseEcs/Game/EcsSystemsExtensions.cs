using System.Collections.Generic;
using System.Linq;
using Leopotam.EcsLite;

namespace Features.BaseEcs.Game
{
    public static class EcsSystemsExtensions
    {
        public static T GetSystem<T>(this IEcsSystems systems)
            where T : IEcsSystem
        {
            foreach (var ecsSystem in systems.GetAllSystems())
            {
                if (ecsSystem is T system)
                {
                    return system;
                }
            }

            return default;
        }
        
        public static IEnumerable<T> GetSystems<T>(this IEcsSystems systems)
            where T : class, IEcsSystem
        {
            foreach (var ecsSystem in systems.GetAllSystems())
            {
                if (ecsSystem is T system)
                {
                    yield return system;
                }
            }
        }
        
        public static bool TryDestroyWithWorlds(this IEcsSystems systems)
        {
            if (systems == null)
            {
                return false;
            }

            var worlds = systems.GetAllNamedWorlds().Values.ToArray();
            systems.Destroy();
            
            foreach (var ecsWorld in worlds)
            {
                ecsWorld.Destroy();
            }
            
            return true;
        }
    }
}