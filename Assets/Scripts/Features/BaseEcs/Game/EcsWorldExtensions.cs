using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using Features.BaseEcs.Data;
using Leopotam.EcsLite;
using UnityEngine.Assertions;

namespace Features.BaseEcs.Game
{
    public static class EcsWorldExtensions
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static IEcsPool GetAndCreatePoolByType(this EcsWorld world, Type type)
        {
            MethodInfo method = typeof(EcsWorld).GetMethod(nameof(EcsWorld.GetPool));
            MethodInfo generic = method.MakeGenericMethod(type);
            return generic.Invoke(world, null) as IEcsPool;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static EcsWorld.Mask FilterByType(this EcsWorld world, Type type)
        {
            MethodInfo method = typeof(EcsWorld).GetMethod(nameof(EcsWorld.Filter));
            MethodInfo generic = method.MakeGenericMethod(type);
            return generic.Invoke(world, null) as EcsWorld.Mask;
        }

        [MethodImpl (MethodImplOptions.AggressiveInlining)]
        public static EcsWorld.Mask IncType (this EcsWorld.Mask mask, Type type) {
            MethodInfo method = typeof(EcsWorld.Mask).GetMethod(nameof(EcsWorld.Mask.Inc));
            MethodInfo generic = method.MakeGenericMethod(type);
            return generic.Invoke(mask, null) as EcsWorld.Mask;
        }
        
        [MethodImpl (MethodImplOptions.AggressiveInlining)]
        public static EcsWorld.Mask ExcType (this EcsWorld.Mask mask, Type type) {
            MethodInfo method = typeof(EcsWorld.Mask).GetMethod(nameof(EcsWorld.Mask.Exc));
            MethodInfo generic = method.MakeGenericMethod(type);
            return generic.Invoke(mask, null) as EcsWorld.Mask;
        }
        
        public static int NewEntity<T>(this EcsWorld world, T component)
            where T : struct
        {
            int entity = world.NewEntity();
            world.GetPool<T>().Add(entity) = component;
            return entity;
        }
        
        public static int NewEntity<T1, T2>(this EcsWorld world, T1 component1, T2 component2)
            where T1 : struct
            where T2 : struct
        {
            int entity = world.NewEntity();
            world.GetPool<T1>().Add(entity) = component1;
            world.GetPool<T2>().Add(entity) = component2;
            return entity;
        }
        
        public static int NewEntityWithComponent<T>(this EcsWorld world, T component)
            where T : struct
        {
            int entity = world.NewEntity();
            world.GetPool<T>().Add(entity) = component;
            return entity;
        }
        
        public static ref T NewEntityWithComponent<T>(this EcsWorld world, EcsPool<T> pool)
            where T : struct
        {
            Assert.AreEqual(pool.GetWorld(), world);
            int entity = world.NewEntity();
            return ref pool.Add(entity);
        }
        
        public static ref T AddComponent<T>(this EcsWorld world, int entity)
            where T : struct
        {
            return ref world.GetPool<T>().Add(entity);
        }
        
        public static ref T NewComponentWithId<T>(this EcsWorld world)
            where T : struct, IEcsComponentWithId
        {
            return ref world.GetPool<T>().NewWithId();
        }
        
        public static ref T GetComponent<T>(this EcsWorld world, int entity)
            where T : struct
        {
            return ref world.GetPool<T>().Get(entity);
        }
        
        public static bool HasComponent<T>(this EcsWorld world, int entity)
            where T : struct
        {
            var pool = world.GetPool<T>();
            return pool.Has(entity);
        }
        
        public static bool TryDelComponent<T>(this EcsWorld world, int entity)
            where T : struct
        {
            var pool = world.GetPool<T>();
            if (!pool.Has(entity))
            {
                return false;
            }

            pool.Del(entity);
            return true;
        }
    }
}