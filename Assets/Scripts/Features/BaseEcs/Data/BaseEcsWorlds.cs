﻿using Leopotam.EcsLite;

namespace Features.BaseEcs.Data
{
    public static class BaseEcsWorlds
    {
        public const string Events = nameof(Events);
        
        public static readonly EcsWorld.Config EventsConfig = new(); 
    }
}