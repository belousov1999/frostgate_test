﻿using Leopotam.EcsLite;

namespace Features.BaseEcs.Data
{
    public interface IEcsDataProvider : IEcsSystem
    {
        public void Init(ContentManager contentManager);
    }
}