﻿namespace Features.BaseEcs.Data
{
    public interface IEventComponent : IEcsComponentWithId
    {
        public string SenderSystem { get; set; } //DEBUG INFO
        public int SenderEntityId { get; set; }
    }
}