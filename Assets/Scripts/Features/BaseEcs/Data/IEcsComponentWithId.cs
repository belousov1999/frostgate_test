
namespace Features.BaseEcs.Data
{
    public interface IEcsComponentWithId
    {
        public int EntityId { get; set; }
    }
}