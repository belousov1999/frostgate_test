using Cysharp.Threading.Tasks;
using Features.BaseEcs.Game;
using Features.BaseSave.Game;
using Features.Player.Game;
using GoodCat.EcsLite.Shared;
using Leopotam.EcsLite;
using OLS.Core;
using OLS.Features.Camera.Game;
using OLS.Features.GameStates.Game;
using OLS.Features.OrdersQueue.Game;
using UnityEngine;

public class MainEntryPoint : MonoBehaviour
{
    private EcsSystems Systems;
    private ContentManager contentManager;

    public async UniTaskVoid Start()
    {
        await InitContentManager();
        InitSystems();
    }

    private void InitSystems()
    {
        Systems = new EcsSystems(new EcsWorld(), new shared.Shared());

        var baseEcsSystemsBuilder = new BaseEcsSystemsBuilder();
        
        var baseSaveSystemsBuilder = new BaseSaveSystemsBuilder();
        var cameraSystemsBuilder = new CameraSystemsBuilder();
        var ordersQueueSystemsBuilder = new OrdersQueueSystemsBuilder();
        var gameStatesSystemsBuilder = new GameStatesSystemsBuilder();
        var playerSystemsBuilder = new UnitSystemsBuilder();

        baseEcsSystemsBuilder.BuildDataProvidersSystems(Systems);
        baseSaveSystemsBuilder.BuildDataProvidersSystems(Systems);
        gameStatesSystemsBuilder.BuildDataProvidersSystems(Systems);
        cameraSystemsBuilder.BuildDataProvidersSystems(Systems);
        ordersQueueSystemsBuilder.BuildDataProvidersSystems(Systems);
        playerSystemsBuilder.BuildDataProvidersSystems(Systems);
            
        baseEcsSystemsBuilder.BuildBaseSystems(Systems);
        baseSaveSystemsBuilder.BuildBaseSystems(Systems);
        gameStatesSystemsBuilder.BuildBaseSystems(Systems);
        cameraSystemsBuilder.BuildBaseSystems(Systems);
        ordersQueueSystemsBuilder.BuildBaseSystems(Systems);
        playerSystemsBuilder.BuildBaseSystems(Systems);
        
        baseEcsSystemsBuilder.BuildMiddleSystems(Systems);
        baseSaveSystemsBuilder.BuildMiddleSystems(Systems);
        gameStatesSystemsBuilder.BuildMiddleSystems(Systems);
        cameraSystemsBuilder.BuildMiddleSystems(Systems);
        ordersQueueSystemsBuilder.BuildMiddleSystems(Systems);
        playerSystemsBuilder.BuildMiddleSystems(Systems);

        baseSaveSystemsBuilder.BuildPostSystems(Systems);
        gameStatesSystemsBuilder.BuildPostSystems(Systems);
        cameraSystemsBuilder.BuildPostSystems(Systems);
        ordersQueueSystemsBuilder.BuildPostSystems(Systems);
        playerSystemsBuilder.BuildPostSystems(Systems);
        baseEcsSystemsBuilder.BuildPostSystems(Systems);

#if UNITY_EDITOR
        var debugFeatureBuilder = new LeoEcsDebugFeatureBuilder();
        debugFeatureBuilder.BuildBaseSystems(Systems);
        debugFeatureBuilder.BuildMiddleSystems(Systems);
        debugFeatureBuilder.BuildPostSystems(Systems);
#endif
            
        Systems.InjectShared(contentManager);

        Systems.InitShared();
        Systems.Init();
    }
        
    private async UniTask InitContentManager()
    {
        contentManager = new ContentManager();
        await contentManager.Init();
        await contentManager.PreloadAssets();
    }

    private void Update()
    {
        Systems?.Run();
    }
}
