using Leopotam.EcsLite;

namespace OLS.Features.FeatureTemplate.Data
{
    public static class FeatureTemplateWorlds
    {
        public const string FeatureTemplate = nameof(FeatureTemplate);

        public static readonly EcsWorld.Config FeatureTemplateConfig = new(); 
    }
}