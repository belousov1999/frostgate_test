using System.Collections.Generic;
using Leopotam.EcsLite;
using OLS.Core;
using OLS.Features.FeatureTemplate.Data;

namespace Features.FeatureTemplate.Game
{
    public class FeatureTemplateSystemsBuilder : BaseFeatureBuilder
    {
        public override Dictionary<EcsWorld, string> GetFeatureWorlds()
        {
            return new Dictionary<EcsWorld, string>()
            {
                [new EcsWorld(FeatureTemplateWorlds.FeatureTemplateConfig)] = FeatureTemplateWorlds.FeatureTemplate,
            };
        }
        
        public override IEcsSystem[] GetDataProviders()
        {
            return new IEcsSystem[]
            {
            };
        }

        public override IEcsSystem[] GetBaseSystems()
        {
            return new IEcsSystem[]
            {
            };
        }

        public override IEcsSystem[] GetMiddleSystems()
        {
            return new IEcsSystem[]
            {
            };
        }

        public override IEcsSystem[] GetPostSystems()
        {
            return new IEcsSystem[]
            {
            };
        }
    }
}
