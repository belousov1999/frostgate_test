﻿using UnityEditor;
using UnityEngine;

namespace Editor
{
    public class PlayerPrefsClearCommand
    {
        [MenuItem("Tools/Clear PlayerPrefs")]
        public static void ClearPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
        }
    }
}